resource "aws_autoscaling_group" "auto_healing" {
  // Auto healing only
  max_size = 1
  min_size = 1
  desired_capacity = 1
  force_delete = false
  termination_policies = [
    "OldestLaunchConfiguration",
    "OldestInstance"
  ]

  // Enable CloudWatch Metrics for group
  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances"
  ]
  metrics_granularity = "1Minute"

  // The ASG name
  name = "${var.name}"

  // Variable parameters
  launch_configuration = "${aws_launch_configuration.auto_healing.id}"
  load_balancers = ["${var.load_balancers}"]
  vpc_zone_identifier = ["${var.subnets}"]
  health_check_type = "${var.health_check_type}"
  wait_for_capacity_timeout = "${var.wait_for_capacity_timeout}"
  health_check_grace_period = "${var.health_check_grace_period}"
  default_cooldown = "${var.default_cooldown}"

  tag {
    key = "Name"
    value = "${var.name}-autoscaled"
    propagate_at_launch = true
  }

  tag {
    key = "AutoHealingConfig"
    value = "${jsonencode(var.auto_healing_config)}"
    propagate_at_launch = false
  }

  tag {
    key = "CSC_MON"
    value = "${var.csc_mon}"
    propagate_at_launch = true
  }

  tag {
    key = "CSC_SEG"
    value = "${var.csc_seg}"
    propagate_at_launch = true
  }

  tag {
    key = "CSC_BKP"
    value = "${var.csc_bkp}"
    propagate_at_launch = true
  }

  tag {
    key                 = "${lookup(var.custom_tag_1, "key")}"
    value               = "${lookup(var.custom_tag_1, "value")}"
    propagate_at_launch = "${lookup(var.custom_tag_1, "propagate_at_launch")}"
  }

  tag {
    key                 = "${lookup(var.custom_tag_2, "key")}"
    value               = "${lookup(var.custom_tag_2, "value")}"
    propagate_at_launch = "${lookup(var.custom_tag_2, "propagate_at_launch")}"
  }

  tag {
    key                 = "${lookup(var.custom_tag_3, "key")}"
    value               = "${lookup(var.custom_tag_3, "value")}"
    propagate_at_launch = "${lookup(var.custom_tag_3, "propagate_at_launch")}"
  }

  tag {
    key                 = "${lookup(var.custom_tag_4, "key")}"
    value               = "${lookup(var.custom_tag_4, "value")}"
    propagate_at_launch = "${lookup(var.custom_tag_4, "propagate_at_launch")}"
  }

  tag {
    key                 = "${lookup(var.custom_tag_5, "key")}"
    value               = "${lookup(var.custom_tag_5, "value")}"
    propagate_at_launch = "${lookup(var.custom_tag_5, "propagate_at_launch")}"
  }

  count = "${var.enable ? 1 : 0}"
}
