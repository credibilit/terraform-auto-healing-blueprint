/* Unit test file for module
 * =========================
 * Declare the folowing variables in environment to execute this code:
 * - AWS_ACCESS_KEY_ID
 * - AWS_SECRET_ACCESS_KEY
 * - AWS_DEFAULT_REGION
 * - AWS_SECURITY_TOKEN (if applicable)
 */

/* AWS account
 * Declare this on TF_VAR_account environment variable with your AWS account id
 * for the test account
 */
variable account {}

// Get the AZs from Amazon
data "aws_availability_zones" "azs" {}

variable "name" {
  default = "autohealing-acme"
}

// A sample VPC
module "vpc" {
  source = "git::ssh://git@bitbucket.org/credibilit/terraform-vpc-blueprint.git?ref=0.0.2"

  account = "${var.account}"
  name = "${var.name}"
  domain_name = "${var.name}.local"
  cidr_block = "10.0.0.0/16"
  azs = "${data.aws_availability_zones.azs.names}"
  az_count = 4
  hosted_zone_comment = "An internal hosted zone for testing autohealing"
  public_subnets_cidr_block = [
    "10.0.0.0/24",
    "10.0.1.0/24",
    "10.0.2.0/24",
    "10.0.3.0/24"
  ]
}

resource "aws_security_group" "ssh" {
  name = "ssh-${var.name}"
  description = "Allow all ssh traffic"
  vpc_id = "${module.vpc.vpc}"

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_key_pair" "seed" {
  key_name = "${var.name}"
  public_key = "${file("keys/sample-keys-do-not-use-in-production.pub")}"
}

data "aws_ami" "ubuntu" {
  most_recent = true
  owners = ["099720109477"]
  filter {
    name = "name"
    values = ["*ubuntu/images/hvm-ssd/ubuntu-trusty-14.04-amd64-server-*"]
  }
}

module "ec2_iam" {
  source = "git::ssh://git@bitbucket.org/credibilit/terraform-ec2-role-blueprint.git?ref=0.0.1"

  account = "${var.account}"
  name = "${var.name}"
}

module "test_auto_healing" {
  source = "../"

  account = "${var.account}"
  name = "${var.name}"
  subnets = ["${module.vpc.public_subnets}"]
  ami = "${data.aws_ami.ubuntu.id}"
  instance_type = "t2.nano"
  associate_public_ip_address = true
  instance_profile = "${module.ec2_iam.instance_profile}"
  security_groups= ["${aws_security_group.ssh.id}"]
  key_pair = "${aws_key_pair.seed.id}"

  auto_healing_config = {
    route = "rt-xxxxxxx"
    eip = "eip-xxxxxxxx"
  }

  csc_mon = "STD"
  csc_seg = "ESS"
  csc_bkp = ""
  custom_tag_1 = {
    key = "Test"
    value = "foo"
    propagate_at_launch = true
  }
}

output "asg" {
  value = "${module.test_auto_healing.asg}"
}

output "asg_arn" {
  value = "${module.test_auto_healing.asg_arn}"
}

output "launch_configuration" {
  value = "${module.test_auto_healing.launch_configuration}"
}

module "test_auto_healing_disabled" {
  source = "../"

  account = "${var.account}"
  name = "${var.name}"
  subnets = ["${module.vpc.public_subnets}"]
  ami = "${data.aws_ami.ubuntu.id}"
  instance_type = "t2.nano"
  associate_public_ip_address = true
  instance_profile = "${module.ec2_iam.instance_profile}"
  security_groups= ["${aws_security_group.ssh.id}"]
  key_pair = "${aws_key_pair.seed.id}"

  auto_healing_config = {
    route = "rt-xxxxxxx"
    eip = "eip-xxxxxxxx"
  }

  csc_mon = "STD"
  csc_seg = "ESS"
  csc_bkp = ""

  enable = false
}
