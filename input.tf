variable "account" {
  type = "string"
  description = "The AWS account number ID"
}

variable "name" {
  type = "string"
  description = "A name to the autoscaling and EC2 Name taggings"
}

variable "wait_for_capacity_timeout" {
  type = "string"
  description = "The time which TerraForm will wait for the resource EC2 creation before continue"
  default = "3m"
}

variable "subnets" {
  type = "list"
  description = "A list containing the subnets to launch the EC2"
}

variable "health_check_grace_period" {
  type = "string"
  description = "Time after instance comes into service before checking health"
  default = "60"
}

variable "health_check_type" {
  type = "string"
  description = "If the ASg must check for EC2 or the ELB attached"
  default = "EC2"
}

variable "default_cooldown" {
  type = "string"
  description = "Time between a scaling activity and the succeeding scaling activity"
  default = 120
}

variable "ami" {
  type = "string"
  description = "The AMI id to launch instance on the auto healing"
}

variable "instance_type" {
  type = "string"
  description = "The EC2 instance type to launch"
}

variable "instance_profile" {
  type = "string"
  description = "The IAM instance profile associate with a EC2 role for the instances to launch"
}

variable "user_data" {
  type = "string"
  description = "The user data to execute on instance start up"
  default = ""
}

variable "enable_monitoring" {
  type = "string"
  description = "If the CloudWatch detailed monitoring must be enable on the instances launched by the auto healing"
  default = "true"
}

variable "ebs_optimized" {
  type = "string"
  description = "If the EBS optimized feature must be enlabled on the instances launched by the auto healing"
  default = "false"
}

variable "associate_public_ip_address" {
  type = "string"
  description = "If the instances must have an random public IP automatically associate with it when launched by the auto healing"
  default = "false"
}

variable "security_groups" {
  type = "list"
  description = "A list of security group ids"
}

variable "key_pair" {
  type = "string"
  description = "The key pair name to associate with the EC2 default user"
}

variable "load_balancers" {
  type = "list"
  description = "A list with all ELB associated with the ASG"
  default = []
}

variable "auto_healing_config" {
  type = "map"
  description = "A map representing a key/pairs set of configurations to keep track on the instance via a Tag"
  default = {}
}

variable "csc_mon" {
  type = "string"
  description = "The CredibiliT tag for monitoring fee"
  default = ""
}

variable "csc_seg" {
  type = "string"
  description = "The CredibiliT tag for security fee"
  default = ""
}

variable "csc_bkp" {
  type = "string"
  description = "The CredibiliT tag for backup fee"
  default = ""
}

variable "enable" {
  description = "Enable the creation of auto healing components"
  default = "true"
}

variable "custom_tag_1" {
  type = "map"
  description = "Custom tag #1"
  default = {
    key = "custom_1"
    value = ""
    propagate_at_launch = false
  }
}

variable "custom_tag_2" {
  type = "map"
  description = "Custom tag #2"
  default = {
    key = "custom_2"
    value = ""
    propagate_at_launch = false
  }
}

variable "custom_tag_3" {
  type = "map"
  description = "Custom tag #3"
  default = {
    key = "custom_3"
    value = ""
    propagate_at_launch = false
  }
}

variable "custom_tag_4" {
  type = "map"
  description = "Custom tag #4"
  default = {
    key = "custom_4"
    value = ""
    propagate_at_launch = false
  }
}

variable "custom_tag_5" {
  type = "map"
  description = "Custom tag #5"
  default = {
    key = "custom_5"
    value = ""
    propagate_at_launch = false
  }
}
