output "asg" {
  value = "${aws_autoscaling_group.auto_healing.id}"
}

output "asg_arn" {
  value = "${aws_autoscaling_group.auto_healing.arn}"
}

output "launch_configuration" {
  value = "${aws_launch_configuration.auto_healing.id}"
}
