resource "aws_launch_configuration" "auto_healing" {
  lifecycle {
    create_before_destroy = true
  }

  name_prefix                 = "${var.name}-"
  image_id                    = "${var.ami}"
  instance_type               = "${var.instance_type}"
  iam_instance_profile        = "${var.instance_profile}"
  user_data                   = "${var.user_data}"
  enable_monitoring           = "${var.enable_monitoring}"
  ebs_optimized               = "${var.ebs_optimized}"
  associate_public_ip_address = "${var.associate_public_ip_address}"
  security_groups             = ["${var.security_groups}"]
  key_name                    = "${var.key_pair}"

  count                       = "${var.enable ? 1 : 0}"
}
