Generic Auto Healing blueprint
===================================

This module configures an Autoscaling Group (**ASG**) to deal with supportive services which cannot hold more than one instance at same time but needs a replacement as soon as possible if the instance became unstable (like **NAT** instances, **Zabbix Servers**, **Jenkins masters**, etc).

Note that, this does not check if a service inside the **EC2** is enabled by default, you need an **ELB** or other method to check it and terminate the fault instance.

This also creates an Launch Configuration (**LC**) to hold the instance data when launching new **EC2** instances. This LC will be recreated if some given parameter change because **AWS** does not permit change LC parameters (you always need to create a new one). Once the auto healing blueprint does not need to keep previous versions of **LC** it only held one **LC** each time.

Also note there is *no support* to manipulate **EBS** or instance stores by this blueprint. This is intentional, leaving this kind of decision to the **AMI**, so *customize your **AWS EC2** image if you need to handle special sets of disks layouts*. read the Custom AMI session bellow for more informations.

This templates does not handle spot instances, since the type of services benefited by the auto healing are not recomended to function on **AWS** spot termination rules.

Finally there is some tags embedded: Name, AutoHealingConfig (see bellow) and the **CredibiliT** tags CSC_MON, CSC_SEG and CSC_BKP. In the current version there is no way to put new tags. This will be handled by future versions

# Architecture

The following image represent the architecture which this blueprint construct.

![architecture](https://bytebucket.org/credibilit/terraform-auto-healing-blueprint/raw/a812403ab05e4e0f62abbd61d6710c3213d793a4/docs/architecture.svg?token=b6c496d1be152b3f8cbcb34ca2951ffbc4641668 "Blueprint resources architecture")

## Subnets layout

Almost always you will pass a list of subnets on `subnets` parameter, this will make the ASG choose one each time it needs to launch a new instance. This is desired to garantee the service will survive *Availability zones (AZ)* outages.

This is not enforced, you actually *can* pass only one subnet to launch a particular service and garantee the live of it in that subnet only. This is desirable when you launch EC2 NAT solutions (like in the [terraform-nat-ec2-simple-blueprint](https://bitbucket.org/credibilit/terraform-nat-ec2-simple-blueprint)) which must serve as gateway for one entire AZ. In this case you will need one auto healing *per* AZ.

## Provisioning

There is a multitude of ways to provision the service running underlying on the launched EC2 instance or attached AWS resources.

You can peek one or more of the following list (which is not exhaustive).

- Using the `user_data` parameter. Usually with an bash script or powershell. This is the simplest, but can lead you to very complex scripts on services with
- Using some configuration manager (**Chef**, **Puppet**, **Ansible**, etc). A lot of considerations must be taken to provide a good solution, we will not elaborate none here, once we are assuming if you are prone to this approach you also have the know how to build this solutions.
- you can use **AWS Lambda** to help attach routes, EIPs or other AWS resources, or even do SSH on the instance and configure some steps inside a EC2.
- Use backed AMI to provisioning is classic on AWS architectures, see the Custom AMI session bellow for a further elaboration. This approach is highly recommended.

### AutoHealingConfig tag

To facilitate the parametrization of the options for auto healing, we define a custom tag `AutoHealingConfig` which can store a JSON to be acquired by the script or configuration manager of your choice to pass values.

For example, you can pass the route tables and Elastic IP like in the [terraform-nat-ec2-simple-blueprint](https://bitbucket.org/credibilit/terraform-nat-ec2-simple-blueprint) to be acquired on the user data and setup the final stage of the launching. So it can assure to always setup the route tables to point the default route to it and have the same external IP for the Internet.

The parameter on the blueprint have this piece of **Terrafrom** code.

```terraform
auto_healing_config = [
  {
    routes = "${join(",", var.private_routes)}"
    eip = "${aws_eip.nat_public_ip.id}"
  }
]
```

This is translated to the following JSON:

```json
{
  "routes": "rtl-XXXXXXXX,rtl-YYYYYYYY",
  "eip": "eip-XXXXXXXX"
}
```

An can be read with the following piece of bash script (sing the `jq` tool) for example:

```bash
ASG=$(aws ec2 describe-tags --region $REGION --filters "Name=resource-id,Values=$INSTANCE_ID" 'Name=key,Values=aws:autoscaling:groupName' | jq '.Tags[0].Value' -r)
CONFIG=$(aws autoscaling describe-tags --region $REGION --filter "Name=auto-scaling-group,Values=$ASG" 'Name=Key,Values=AutoHealingConfig' | jq '.Tags[0].Value' -r)

EIP_ID=$(echo $CONFIG | jq -r '.eip')
ROUTE_TABLES=$(echo $CONFIG | jq -r '.routes' | sed 's/,/ /g')
```

Once this is created by the `jsonencode()` interpolation function over the content of the `auto_healing_config` parameter do not expect to pass a complex map and be interpreted 100% correctly, avoid passing sub maps or lists inside the keys values.

### Custom AMI

When using backed AMI, you can use two approaches from the AWS best practices:

- *Golden AMI*: where you pack everything on the AMI.
- *Silver AMI*: where you pack almost everything, leaving particular configurations on the hands of a configuration manager or script.

The first one will provide a faster boot, but need a lot of effort on the AMI creation. The second one provide a more customizable environment, also it can handle a few cases where the effort to make a dynamic provisioning on the AMI backing phase is too steep or even impossible.

There is some blueprints for both cases to create specialized AMI with [Packer](https://www.packer.io/) in the project which this repository belongs on **BitBucket**. Seek for it or create one for your purposes.

Like mentioned on the introduction this is particular useful to provide custom disks layouts once this is not handled (intentionally) by this module.

Also, the time of provisioning can be very high (as on **Jenkins** blueprint), using a backed AMI to provide most of the effort will make the outage time of your service as low as possible once the creation of the .

## Elastic Load Balancers

You can optionally pass one or more **Elastic Load Balancers (ELB)** to attach to the ASG. This is particular useful when the service running behind the curtain is HTTP/HTTPS based or at least TCP based, once allow you to check if the service itself is up.

Another advantage of using ELB is a single endpoint for other services without the need to change DNS registers or attach IP addresses directly to EC2 instances.

# Use

To create the auto healing with this module you need to insert the following peace of code on your own modules:

```
// Call the module
module "<YOUR_MODULE_NAME>" {
  source = "git::ssh://git@bitbucket.org/credibilit/terraform-auto-healing-blueprint.git?ref=<VERSION>"

  ... <parameters> ...
}
```

Where `<VERSION>` is the desired version of *this* module. The master branch store the list of versions which can be used. The possible parameters are listed in advance on this document.

## Dependencies

This blueprint does not call another blueprints, so do not have any internal dependency. But you need some resources created previously on the parameter bellow (key pairs, VPC, subnets, EC2 instance profiles, etc).

In particular you will need a backed custom AMI to launch the services easily.

# Parameters

This is the list of parameters accepted and exposed by the module.

## Input Parameters

The following parameters are used on this module:

- `account`: The AWS account number ID.
- `enable`: Enable the creation of auto healing components
- `name`: A name to the autoscaling and EC2 Name taggings
- `wait_for_capacity_timeout`: The time which TerraForm will wait for the resource EC2 creation before continue (default: "3m")
- `subnets`: A list containing the subnets to launch the EC2
- `health_check_grace_period`: Time after instance comes into service before checking health (default: "60")
- `health_check_type`: If the ASG must check for EC2 or the ELB attached (default: "EC2")
- `default_cooldown`: Time between a scaling activity and the succeeding scaling activity (default: 120)
- `ami`: The AMI id to launch instance on the auto healing
- `instance_type`: The EC2 instance type to launch
- `instance_profile`: The IAM instance profile associate with a EC2 role for the instances to launch
- `user_data`: The user data to execute on instance start up (default: "")
- `enable_monitoring`: If the CloudWatch detailed monitoring must be enable on the instances launched by the auto healing (default: "true")
- `ebs_optimized`: If the EBS optimized feature must be enlabled on the instances launched by the auto healing (default: "false")
- `associate_public_ip_address`: If the instances must have an random public IP automatically associate with it when launched by the auto healing (default: "false")
- `security_groups`: A list of security group ids
- `key_pair`: The key pair name to associate with the EC2 default user
- `load_balancers`: An optional list with all ELB associated with the ASG (default: none).
- `auto_healing_config`: A map representing a key/pairs set of configurations to keep track on the instance via a Tag (default: empty).
- `csc_mon`: The CredibiliT tag for monitoring fee (default: "").
- `csc_seg`: The CredibiliT tag for security fee (default: "").
- `csc_bkp`: The CredibiliT tag for backup fee (default: "").
- `custom_tag_1`: The Custom Tag #1
- `custom_tag_2`: The Custom Tag #2
- `custom_tag_3`: The Custom Tag #3
- `custom_tag_4`: The Custom Tag #4
- `custom_tag_5`: The Custom Tag #5

## Output parameters

This are the outputs exposed by this module.

- `asg`: The autoscaling group created to handle the auto healing events
- `asg_arn`: The autoscaling group ARN
- `launch_configuration`: The launch configuration created to help the ASG to handle auto healing events.

# TODO

- Better tagging support
